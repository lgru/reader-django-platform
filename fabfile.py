from fabric.api import run, cd, env, prefix, local


def lgru():
    env.hosts = ['constant@lgru.net:222']
    env.path = '/home/constant/www/reader.lgru.net/www/run/'


def deploy():
    with cd(env.path):
        run('git pull origin master')

        with prefix('source /home/constant/www/reader.lgru.net/venv/bin/activate'):
            run('python manage.py collectstatic --noinput')

    run('touch /home/constant/www/reader.lgru.net/wsgi.py') 


def download():
    """synchronizes the local db and media files from the remote ones"""
    foo = '/home/constant/www/reader.lgru.net/'
    local('scp -P 222 constant@lgru.net:%slgru.db run/' % foo)
    local("rsync -e 'ssh -p 222' -avz --progress --stats constant@lgru.net:%smedia run/" % foo)
