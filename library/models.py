from django.utils.translation import ugettext as _
from django.db import models
from django.template.defaultfilters import slugify
from publications.models import Publication
from hvad.models import TranslatableModel, TranslatedFields


STATUS_SELECTED = 0
STATUS_FURTHER_READING = 1
STATUS_MAYBE = 2


STATUS_CHOICES = (
    (STATUS_SELECTED, "Selected"),
    (STATUS_FURTHER_READING, "Further reading"),
    (STATUS_MAYBE, "Maybe"),
)

class License(models.Model):
    """
    Represents a license.
    """
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    body = models.TextField(blank=True)
    
    def __unicode__(self):
        return self.name

class Language(models.Model):
    name = models.CharField(max_length=25)
    iso_code = models.CharField(max_length=2)
    def __unicode__(self):
        return self.name

class Text(models.Model):
    """
    Represents a text.
    """
    reference = models.ForeignKey(Publication)
    slug = models.SlugField(max_length=255, blank=True)
    body = models.TextField(blank=True)
    license = models.ForeignKey(License)
    
    def __unicode__(self):
        return self.reference.title

    class Meta:
        ordering = ['slug']
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.reference.title)
        super(Text, self).save(*args, **kwargs)

    @models.permalink
    def get_absolute_url(self):
         return ('library-text-detail', [self.slug])

class Translation(models.Model):
    """
    Represents a translation relationship.
    """
    text = models.ForeignKey(Text)
    language = models.ForeignKey(Language)
    translation = models.ForeignKey(Publication)

class Image(models.Model):
    text = models.ForeignKey(Text)
    caption = models.CharField(max_length=255)
    image = models.FileField(upload_to="images", max_length=255, verbose_name='image file')

class Collection(models.Model):
    """
    Represents a collection.
    """
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    body = models.TextField(blank=True)
    texts = models.ManyToManyField(Text, through="Membership")
    order = models.PositiveIntegerField(blank=True, null=True)
    
    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['order']
    
    @models.permalink
    def get_absolute_url(self):
        return ('library-collection-detail', [self.slug])


class Membership(models.Model):
    text = models.ForeignKey(Text)
    collection = models.ForeignKey(Collection)
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=0)
    order = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ['order']
    
    def __unicode__(self):
        return self.text.reference.title


class Type(TranslatableModel):
    slug = models.SlugField(max_length=255, blank=True)
    translations = TranslatedFields(
        name = models.CharField(max_length=255),
    )



class Entry(TranslatableModel):
    MONTH_CHOICES = (
        ( 1, _('January')),
        ( 2, _('February')),
        ( 3, _('March')),
        ( 4, _('April')),
        ( 5, _('May')),
        ( 6, _('June')),
        ( 7, _('July')),
        ( 8, _('August')),
        ( 9, _('September')),
        (10, _('October')),
        (11, _('November')),
        (12, _('December'))
    )
    slug = models.SlugField(max_length=255, blank=True)
    type = models.ForeignKey(Type)
    authors = models.CharField(max_length=2048, help_text='List of authors separated by commas or <i>and</i>.')

    translations = TranslatedFields(
        title = models.CharField(max_length=512),
        book_title = models.CharField(max_length=256, blank=True),

        license = models.ForeignKey(License, blank=True, null=True),

        year = models.PositiveSmallIntegerField(max_length=4),
        month = models.PositiveSmallIntegerField(choices=MONTH_CHOICES, blank=True, null=True),

        abstract = models.TextField(blank=True),
        body = models.TextField(blank=True),

        editors = models.CharField(max_length=2048, blank=True, help_text=""),
        publisher = models.CharField(max_length=256, blank=True),

        pdf = models.FileField(upload_to='publications/', verbose_name='PDF', blank=True, null=True),
        url = models.URLField(blank=True, verify_exists=False, verbose_name='URL', help_text='Link to PDF or journal page.'),
        volume = models.IntegerField(blank=True, null=True),
        number = models.IntegerField(blank=True, null=True, verbose_name='Issue number'),
        #pages = PagesField(max_length=32, blank=True),
        pages = models.CharField(max_length=32, blank=True),
        journal = models.CharField(max_length=256, blank=True),
    )

    #def __unicode__(self):
        #return self.title

    class Meta:
        pass
        #app_label = _('publications')
        #ordering = ['-year', '-month', '-id']
