from django.core.management.base import BaseCommand, CommandError

import requests
from publications.models import Publication, Type
from publications.models import Publication, Type
from library.models import Text
from django.template.defaultfilters import slugify


class Command(BaseCommand):
    #args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        r = requests.get("http://localhost:5984/lgru/_design/lgru/_view/publications")

        json = r.json


        def natural_join(val):
            if isinstance(val, list):
                return " ".join((", ".join(val[0:-1]), "and %s" % val[-1])) if len(val) > 1 else val[0]
            else:
                return val


        type = Type.objects.all()[0]

        for row in json["rows"]:
            value = row["value"]["bibtex"]

            pub = Publication()
            pub.type = type

            for field in value.keys():
                if field == "abstract":
                    pub.abstract = value[field]
                elif field == "author" or field == "authors":
                    pub.authors = natural_join(value[field])
                elif field == "book_title" or field == "book":
                    pub.book_title = value[field]
                #elif field == "collection":
                    #pub.collection = value[field]  # !!!
                elif field == "editor" or field == "editors":
                    pub.editors = value[field]
                elif field == "issue":
                    pub.number= value[field]
                elif field == "journal":
                    pub.journal = value[field]
                elif field == "language":
                    pub.language = value[field]
                #elif field == "licence" or field == "license":
                    #pub.license = value[field]
                elif field == "month":
                    pub.month = value[field]
                elif field == "number":
                    pub.number = value[field]
                elif field == "pages":
                    pub.pages = value[field]
                elif field == "publisher":
                    pub.publisher = value[field].strip()
                elif field == "title":
                    pub.title = value[field]
                elif field == "translators":
                    pub.translator = natural_join(value[field])
                elif field == "url":
                    if isinstance(value[field], list):
                        pub.url = value[field][0]
                    else:
                        pub.url = value[field]
                elif field == "volume":
                    pub.volume = value[field]
                elif field == "year":
                    if isinstance(value[field], unicode):
                        pub.year = int(value[field].split("-")[0])
                    else:
                        pub.year = value[field]

            if not pub.year:
                pub.year = 0

            pub.save()

            text = Text()
            if "body" in row["value"]:
                text.body = row["value"]["body"]

            text.reference = pub
            text.slug = slugify(pub.title)
            text.save()
