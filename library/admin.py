from django.contrib import admin
from library.models import (License, Text, Collection, Membership, Translation, Language, Image, Entry, Type)
from orderable_inlines import OrderableTabularInline
from hvad.admin import TranslatableAdmin


class MembershipInline(OrderableTabularInline):
    model = Membership
    extra = 0
    orderable_field = 'order'


class LicenseAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}


class ImageInline(OrderableTabularInline):
    model = Image
    extra = 0
    orderable_field = 'order'


class TranslationInline(OrderableTabularInline):
    model = Translation
    extra = 0
    orderable_field = 'order'


class TextAdmin(admin.ModelAdmin):
    inlines = (TranslationInline, ImageInline)
    pass


class CollectionAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    filter_vertical = ("texts",)
    inlines = (MembershipInline,)


class EntryAdmin(admin.ModelAdmin):
    pass


admin.site.register(License, LicenseAdmin)
admin.site.register(Text, TextAdmin)
admin.site.register(Language)
admin.site.register(Collection, CollectionAdmin)
admin.site.register(Entry, TranslatableAdmin)
admin.site.register(Type, TranslatableAdmin)
