from django.conf.urls.defaults import *
from django.views.generic import list_detail
from library.models import (Text, Collection, Entry)


text_dict = {
    'queryset' : Text.objects.all(),
}

raw_text_dict = {
    'queryset' : Text.objects.all(),
    'template_name': 'library/raw_text_detail.html',
    'mimetype': 'text/plain'
}

collection_dict= {
    'queryset' : Collection.objects.all(),
}

entries_dict = {
    'queryset' : Entry.objects.all(),
}

urlpatterns = patterns('',
    url(r'^$', 'django.views.generic.simple.redirect_to', {'url': '/pages/index/'}),
    url(r'^texts/$', list_detail.object_list, text_dict, 'library-text-list'),
    url(r'^texts/(?P<slug>[-\w]+).md/$', list_detail.object_detail, raw_text_dict, 'library-raw-text-detail'),
    url(r'^texts/(?P<slug>[-\w]+)/$', list_detail.object_detail, text_dict, 'library-text-detail'),

    url(r'^entries/(?P<slug>[-\w]+)/$', list_detail.object_detail, entries_dict, 'library-entry-detail'),

    url(r'^collections/$', list_detail.object_list, collection_dict, 'library-collection-list'),
    url(r'^collections/(?P<slug>[-\w]+)/$', list_detail.object_detail, collection_dict, 'library-collection-detail')
)
