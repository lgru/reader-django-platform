from django.conf import settings
from django.conf.urls.defaults import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin


# Uncomment the next two lines to enable the admin:
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    ('^pages/', include('django.contrib.flatpages.urls')),
    url(r'^publications/', include('publications.urls')),
    #url(r'^annotator/', include('annotator.urls')),
    url(r'^', include('library.urls')),
    url(r'^admin/publications/publication/import_bibtex/$', 'publications.admin_views.import_bibtex')
)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT, show_indexes=True)
